package com.retrogradelab.datalize.repository;

import com.retrogradelab.datalize.dto.*;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.entity.Case;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import com.retrogradelab.datalize.dto.Suspects;
import org.springframework.data.repository.query.Param;

public interface CDRRepository extends JpaRepository<CDR, Integer> {

    List<CDR> findAllByACase(Case c);
    List<CDR> findAllByACaseOrderByDate(Case c);

    @Query(
            "SELECT "+
                    "new com.retrogradelab.datalize.dto.Suspects(count(cdr.id), cdr.receiver,cdr.cellId, cdr.lac) from CDR as cdr WHERE a_case_id=:caseId and length(cdr.receiver) > 6 GROUP BY receiver,cellId,lac"
    )
    List<Suspects> suspectedCallsByCid(@Param("caseId") int caseId);


    @Query(
            "SELECT "+
                    "new com.retrogradelab.datalize.dto.Suspects(count(cdr.id), cdr.receiver) from CDR as cdr WHERE a_case_id=:caseId and length(cdr.receiver) > 6 GROUP BY receiver"
    )
    List<Suspects> suspectedCallsByCidGroupByReceiver(@Param("caseId") int caseId);

    List<CDR> findAllByReceiverAndACaseAndLacAndCellIdOrderByDate(String receiver, Case caseId, String lac, String cellId);

    @Query(value = "select cdr.id, cdr.callerphone, cdr.receiverphone, cdr.a_case_id, cdr.call_duration, cdr.address, cdr.imei, cdr.lac, cdr.time, cdr.type, c.latitude, c.longitude, cdr.cell_id from calldetailsrecord cdr inner join cell_id c on(cdr.cell_id = c.cell_id) where length(cdr.receiverphone) > 6 and cdr.a_case_id =:caseId", nativeQuery = true)
    List<CDR> findAllByACaseAndGroupByCellIdOrderByDate(@Param("caseId") int caseId);

    @Query("select " +
            "distinct new com.retrogradelab.datalize.dto.LinkAnalysis(c.usageType,c.caller, c.receiver) from CDR as c where length(c.receiver) > 6 and a_case_id=:caseId")
    List<LinkAnalysis> getLinkAnalyisiData(@Param("caseId") int caseId);

    @Query("select " +
            "distinct new com.retrogradelab.datalize.dto.IMEIReportDto(count(c.caller), c.imei, c.receiver) from CDR as c where length(c.receiver) > 6 and a_case_id=:caseId and c.caller=:caller group by c.imei,c.receiver order by count(c.caller) desc ")
    List<IMEIReportDto> getImeiReport(@Param("caseId") int caseId, @Param("caller") String caller);

    List<CDR> findAllByACaseAndReceiverAndImeiOrderByDate(Case aCase, String receiver, String imei);

    List<CDR> findAllByACaseAndCallerAndReceiverOrderByDate(Case aCase, String caller, String receiver);

    @Query(value = "SELECT cdr.id, cdr.callerphone, cdr.receiverphone, cdr.a_case_id, cdr.call_duration, cdr.address, cdr.imei, cdr.lac, cdr.time, cdr.type, cdr.latitude, cdr.longitude, cdr.cell_id from calldetailsrecord cdr WHERE a_case_id =:caseId and call_duration =:i order by cdr.date", nativeQuery = true)
    List<CDR> findAllByACaseAndCallDuration(@Param("caseId") int caseId, @Param("i") double i);

    @Query(value = "select distinct new com.retrogradelab.datalize.dto.AddressDTO(c.address) from CDR c where a_case_id=:caseId and length(c.receiver) >5")
    List<AddressDTO> getDistinctAddressByACase(@Param("caseId") int caseId);

    List<CDR> findAllByACaseAndAddressOrderByDate(Case one, String token);

    @Query(value = "SELECT * FROM calldetailsrecord c where call_duration >= 0 and c.type not like \"%SMS%\" and a_case_id =:caseId order by call_duration desc", nativeQuery = true)
    List<CDR> findAllByDurationAndCase(@Param("caseId") int caseId);


    @Query(value = "select new com.retrogradelab.datalize.dto.AddressDTO(count(c.receiver), c.caller, c.receiver, c.address) from CDR c where a_case_id =:caseId and length(c.receiver) > 6 group by c.receiver,c.caller,c.address order by count(c.receiver) desc ")
    List<AddressDTO> frequentCallAddress(@Param("caseId") int caseId);
}
