package com.retrogradelab.datalize.repository;

import com.retrogradelab.datalize.entity.CELLID;
import com.retrogradelab.datalize.entity.Case;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CELLIDRepository extends JpaRepository<CELLID, Integer> {

    List<CELLID> findCELLIDByACase(Case acase);
}
