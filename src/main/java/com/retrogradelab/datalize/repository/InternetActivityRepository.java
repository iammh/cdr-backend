package com.retrogradelab.datalize.repository;

import com.retrogradelab.datalize.entity.Case;
import com.retrogradelab.datalize.entity.InternetActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InternetActivityRepository extends JpaRepository<InternetActivity, Integer> {

    List<InternetActivity> findAllByACase(Case aCase);
}
