package com.retrogradelab.datalize.bean;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

import javax.servlet.annotation.MultipartConfig;

@EnableAutoConfiguration
@MultipartConfig
public class Multipart {
}
