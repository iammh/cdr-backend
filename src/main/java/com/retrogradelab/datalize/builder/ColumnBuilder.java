package com.retrogradelab.datalize.builder;

import com.retrogradelab.datalize.constants.*;

import java.util.HashMap;
import java.util.Map;

public class ColumnBuilder {

    public static ColumnIndex build(Types TYPE) {
        ColumnIndex columnIndex = null;
        if(TYPE.equals(Types.AIRTEL)) {
            Map<String, Integer> airtelColumn = new HashMap<>();
            airtelColumn.put(ColumnNames.DATE.name(), AirtelCellType.DATE.getIndex());
            airtelColumn.put(ColumnNames.CALLER.name(), AirtelCellType.CALLER.getIndex());
            airtelColumn.put(ColumnNames.RECEIVER.name(), AirtelCellType.RECEIVER.getIndex());
            airtelColumn.put(ColumnNames.DURATION.name(), AirtelCellType.DURATION.getIndex());
            airtelColumn.put(ColumnNames.USAGE_TYPE.name(), AirtelCellType.USAGE_TYPE.getIndex());
            airtelColumn.put(ColumnNames.LAC.name(), AirtelCellType.LAC.getIndex());
            airtelColumn.put(ColumnNames.CELL.name(), AirtelCellType.CELL.getIndex());
            airtelColumn.put(ColumnNames.IMEI.name(), AirtelCellType.IMEI.getIndex());
            airtelColumn.put(ColumnNames.ADDRESS.name(), AirtelCellType.ADDRESS.getIndex());
            columnIndex = new AirtelIndex(airtelColumn);

        }
        else if(TYPE.equals(Types.BANGLALINK)) {
            Map<String, Integer> banglaLink = new HashMap<>();
            banglaLink.put(ColumnNames.DATE.name(), BanglaLinkCellType.DATE.getIndex());
            banglaLink.put(ColumnNames.CALLER.name(), BanglaLinkCellType.CALLER.getIndex());
            banglaLink.put(ColumnNames.RECEIVER.name(), BanglaLinkCellType.RECEIVER.getIndex());
            banglaLink.put(ColumnNames.DURATION.name(), BanglaLinkCellType.DURATION.getIndex());
            banglaLink.put(ColumnNames.USAGE_TYPE.name(), BanglaLinkCellType.USAGE_TYPE.getIndex());
            banglaLink.put(ColumnNames.LAC.name(), BanglaLinkCellType.LAC.getIndex());
            banglaLink.put(ColumnNames.CELL.name(), BanglaLinkCellType.CELL.getIndex());
            banglaLink.put(ColumnNames.IMEI.name(), BanglaLinkCellType.IMEI.getIndex());
            banglaLink.put(ColumnNames.ADDRESS.name(), BanglaLinkCellType.ADDRESS.getIndex());
            columnIndex = new BanglaLinkIndex(banglaLink);
        }
        else if(TYPE.equals(Types.GRAMEENPHONE)) {
            Map<String, Integer> grameen = new HashMap<>();
            grameen.put(ColumnNames.DATE.name(), GrameenCellType.DATE.getIndex());
            grameen.put(ColumnNames.CALLER.name(), GrameenCellType.CALLER.getIndex());
            grameen.put(ColumnNames.RECEIVER.name(), GrameenCellType.RECEIVER.getIndex());
            grameen.put(ColumnNames.DURATION.name(), GrameenCellType.DURATION.getIndex());
            grameen.put(ColumnNames.USAGE_TYPE.name(), GrameenCellType.USAGE_TYPE.getIndex());
            grameen.put(ColumnNames.LAC.name(), GrameenCellType.LAC.getIndex());
            grameen.put(ColumnNames.CELL.name(), GrameenCellType.CELL.getIndex());
            grameen.put(ColumnNames.IMEI.name(), GrameenCellType.IMEI.getIndex());
            grameen.put(ColumnNames.ADDRESS.name(), GrameenCellType.ADDRESS.getIndex());
            columnIndex = new GrameenIndex(grameen);
        }else if(TYPE.equals(Types.AIRTEL_CELL)) {

            Map<String, Integer> airtelCell = new HashMap<>();


            columnIndex = new AirtelCellColumnIndex(airtelCell);

        }

        return columnIndex;
    }
}
