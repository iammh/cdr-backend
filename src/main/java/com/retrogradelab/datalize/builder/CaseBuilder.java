package com.retrogradelab.datalize.builder;

import com.retrogradelab.datalize.dto.CaseDto;
import com.retrogradelab.datalize.entity.Case;

public class CaseBuilder {

    public static Case build(CaseDto caseDto) {
        Case ca = new Case();

        ca.setName(caseDto.getName());
        ca.setInvestigator(caseDto.getInvestigator());
        ca.setCaseDate(caseDto.getCaseDate());
        ca.setFileDate(caseDto.getFileDate());
        ca.setAsignee(caseDto.getAsignee());

        return ca;
    }
}
