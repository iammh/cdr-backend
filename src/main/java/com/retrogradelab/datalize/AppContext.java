package com.retrogradelab.datalize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ComponentScan(basePackages = "com.retrogradelab")
@PropertySource("classpath:datalize.properties")
public class AppContext extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    System.setProperty("multipart.max-file-size", "100MB");
    System.setProperty("multipart.max-request-size", "100MB");
    return builder.sources(AppContext.class);
  }

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurerAdapter() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
          .allowedOrigins("*")
          .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD")
          .allowCredentials(true);
      }
    };
  }



  public static void main(String[] args) {
    System.setProperty("multipart.max-file-size", "100MB");
    System.setProperty("multipart.max-request-size", "100MB");
    SpringApplication.run(AppContext.class, args);
  }
}
