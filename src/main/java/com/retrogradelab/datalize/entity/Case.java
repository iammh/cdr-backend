package com.retrogradelab.datalize.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "cdr_case")
@Data
public class Case implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String investigator;

    @Column
    private String asignee;

    @Column
    private Date fileDate;

    @Column
    private Date caseDate;

}
