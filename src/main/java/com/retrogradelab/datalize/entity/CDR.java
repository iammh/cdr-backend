package com.retrogradelab.datalize.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "calldetailsrecord")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CDR implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "callerphone", nullable = false)
    private String caller;

    @Column(name = "receiverphone", nullable = false)
    private String receiver;

    @Column(name = "time")
    private Date date;

    @Column(name = "call_duration")
    private double callDuration;

    @Column(name = "type")
    private String usageType;

    @Column(name = "cell_id")
    private String cellId;

    @Column(name = "imei")
    private String imei;

    @Lob
    @Column(name = "address", length = 500)
    private String address;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "lac")
    private String lac;

    @OneToOne(fetch = FetchType.EAGER)
    private Case aCase;

}
