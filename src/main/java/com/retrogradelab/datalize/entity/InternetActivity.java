package com.retrogradelab.datalize.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "internet_activity")
@Data
public class InternetActivity {

  @GeneratedValue(strategy = GenerationType.AUTO)
  @Id
  private int id;

  @Column
  private String session;

  @Column
  private String publicIp;

  @Column
  private String publicPort;

  @Column
  private String MSISDN;

  @Column
  private String applicationType;

  @Column
  private String url;

  @Column
  private String msIp;

  @Column
  private String serverIp;
//  private String natBeginTime;
//  private String natEndime;
//
//  private String startTime;
//  private String endTime;
//  private String IMSI;
//  MSISDN	IMEISV	MS IP	Server IP 	MS Port	Server Port	CGI	SAI	ECGI	Uplink Traffic(B)	Downlink Traffic(B)	Category Type	Application Type	URL

  @OneToOne(fetch = FetchType.EAGER)
  private Case aCase;
}
