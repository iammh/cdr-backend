package com.retrogradelab.datalize.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@JsonIgnoreProperties
@Embeddable
public class Location implements Serializable{

  @Column
  private String address;

  @Column
  private String street;

  @Column
  private String division;

  @Column
  private String district;

  @Column
  private String thana;


  public String fullAddress () {
    return address + ", "+ street +" , "+ thana + " , "+ district + " , "+ division;
  }
}
