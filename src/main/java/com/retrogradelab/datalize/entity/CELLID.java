package com.retrogradelab.datalize.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cell_id")
@Data
public class CELLID implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(name = "cell_id")
  private String cellId;

  @Column(name = "lac")
  private String lac;

  @Column(name = "site_name")
  private String siteName;

  @Column(name = "cell_name")
  private String cellName;

  @Column(name = "sector")
  private String sector;

  @Column(name = "cgi")
  private String cgi;

  @Column
  private double longitude;

  @Column
  private double latitude;

  @Column
  private int orientation;

  @Column
  private String typeOfRbs;

  @Embedded
  private Location location;

  @OneToOne
  private Case aCase;

}
