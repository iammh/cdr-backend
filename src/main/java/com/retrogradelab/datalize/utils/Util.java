package com.retrogradelab.datalize.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String doubleToString(double documentNumber) {
        NumberFormat nf = DecimalFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        nf.setGroupingUsed(false);
        String str = nf.format(documentNumber);

        return str;
    }

    public static Date strToDate(String str) {
        Date d = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
            d = simpleDateFormat.parse(str);
        }catch (Exception ex) {
            d=null;
        }

        return d;
    }
}
