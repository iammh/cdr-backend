package com.retrogradelab.datalize.service;

import com.retrogradelab.datalize.dao.CDRDao;
import com.retrogradelab.datalize.dto.*;
import com.retrogradelab.datalize.dto.v2.Connection;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.entity.CELLID;
import com.retrogradelab.datalize.entity.Case;
import com.retrogradelab.datalize.entity.InternetActivity;
import com.retrogradelab.datalize.processor.CellParser;
import com.retrogradelab.datalize.processor.IPParser;
import com.retrogradelab.datalize.repository.CDRRepository;
import com.retrogradelab.datalize.repository.CELLIDRepository;
import com.retrogradelab.datalize.repository.CaseRepository;
import com.retrogradelab.datalize.repository.InternetActivityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CaseService {

    private final static Logger logger = LoggerFactory.getLogger(CaseService.class);

    @Autowired
    private CaseRepository caseRepository;

    @Autowired
    private CDRRepository cdrRepository;

    @Autowired
    private CELLIDRepository cellidRepository;

    @Autowired
    private CDRDao cdrDao;

    @Autowired
    private InternetActivityRepository internetActivityRepository;

    public Case create(Case aCase) {
        return caseRepository.save(aCase);
    }

    public List<Case> findAll() {
        return caseRepository.findAll();
    }

    public void storeCRD(int id, String originalFilename, String contentType, List<Record> records) {
        Case ca = caseRepository.getOne(id);
        for (Record record:records) {
            CDR cdr = record.buildCDR();
            cdr.setACase(ca);
            cdrRepository.save(cdr);
        }
    }

    public List<CDR> getCRDRecordsByCase(int caseId) {
        Case c = caseRepository.getOne(caseId);
        return cdrRepository.findAllByACaseOrderByDate(c);
    }

    public List<CELLID> storeCell(int cid, int operator, MultipartFile file) {

        Case ca = caseRepository.getOne(cid);
        List<CellIdRecord> records = new ArrayList<>();
        CellParser cellParser =  new CellParser();

        try {
            cellParser.parse(file, records);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<CELLID> re = new ArrayList<CELLID>();

        for (CellIdRecord cRecord:records) {

            CELLID cellid = cRecord.build();

            cellid.setACase(ca);

            cellidRepository.save(cellid);

            re.add(cellid);

        }
        return re;

    }

    public List<CDR> searchByCriteria(List<SearchPayload> loads) {
        logger.info("MHLOG:: AT SERVICE "+ loads);
        return cdrDao.searchByCriteria(loads);
    }

    public List<CELLID> getCellId(int cid) {
        return cellidRepository.findCELLIDByACase(caseRepository.getOne(cid));
    }

    public List<CDR> getNightCallsByCid(int cid) {
        List<CDR> cases = this.getCRDRecordsByCase(cid);
        List<CDR> casesHour = new ArrayList<>();



        for (CDR cdr:cases) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime( cdr.getDate());

            int hour = calendar.get(Calendar.HOUR_OF_DAY);

            if( hour >= 22 || (hour > 0 && hour < 7)) {
                casesHour.add(cdr);

            }

        }

        return getCalls(casesHour);
    }

    public List<CDR> getTimedCallsByCid(int cid, String time) {
        List<CDR> cases = this.getCRDRecordsByCase(cid);
        List<CDR> casesHour = new ArrayList<>();


        if(time != null && time.equals("day")) {

            for (CDR cdr : cases) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cdr.getDate());

                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                if (hour >= 7 && hour < 23) {
                    casesHour.add(cdr);

                }

            }
        } else if (time != null && time.equals("office")){

            for (CDR cdr : cases) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cdr.getDate());

                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                if (hour >= 8 && hour <= 17) {
                    casesHour.add(cdr);

                }

            }
        } else if (time != null && time.equals("evening")){

            for (CDR cdr : cases) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(cdr.getDate());

                int hour = calendar.get(Calendar.HOUR_OF_DAY);

                if (hour >= 16 && hour <= 20) {
                    casesHour.add(cdr);

                }

            }
        }

        return getCalls(casesHour);
    }

    public Suspects getSuspectedCallsByCid(int cid) {
        List<CDR> cases = this.getCRDRecordsByCase(cid);
        List<CDR> casesHour = new ArrayList<>();

        List<Suspects> suspects = cdrRepository.suspectedCallsByCid(cid);

        Suspects suspected = null;

        for (Suspects suspect: suspects) {
            if(suspected == null) {
                suspected = suspect;
            }else {
                if(suspected.getCount() < suspect.getCount()) {
                    suspected = suspect;
                }
            }
        }


//        return cdrDao.suspectedCallsByCid(cid);
        return suspected;
    }


    public List<CDR> getSuspects(int caseId, String lac, String cellId, String receiver) {

        List<CDR> suspectedCallsOrSms = cdrRepository.findAllByReceiverAndACaseAndLacAndCellIdOrderByDate(receiver, caseRepository.getOne(caseId), lac, cellId);


        Iterator<CDR> iter = suspectedCallsOrSms.iterator();

        while (iter.hasNext()) {
            CDR cdr = iter.next();

            if(cdr.getUsageType().contains("SMS")) {
                iter.remove();
            }
        }



        return suspectedCallsOrSms;
    }

    private List<CDR> getCalls(List<CDR> callRecords) {

        Iterator<CDR> iter = callRecords.iterator();

        while (iter.hasNext()) {
            CDR cdr = iter.next();

            if(cdr.getUsageType().contains("SMS")) {
                iter.remove();
            }
        }

        return callRecords;
    }

    private List<CDR> getSMS(List<CDR> callRecords) {

        Iterator<CDR> iter = callRecords.iterator();

        while (iter.hasNext()) {
            CDR cdr = iter.next();

            if(!cdr.getUsageType().contains("SMS")) {
                iter.remove();
            }
        }

        return callRecords;
    }


    public List<Suspects> getSuspectedCallsFrequencyByCase(int cid) {

        return cdrRepository.suspectedCallsByCidGroupByReceiver(cid);
    }

    public List<CDR> findAllByACaseAndGroupByCellId(int caseId) {
        return cdrRepository.findAllByACaseAndGroupByCellIdOrderByDate(caseId);
    }

    public Netwok getAnalysis(int caseId) {
        List<LinkAnalysis> analyisiData = cdrRepository.getLinkAnalyisiData(caseId);

        Netwok netwoks = new Netwok();


        if(analyisiData.size() > 0) {

            List<Node> nodes = new ArrayList<>();
            List<Edge> edges = new ArrayList<>();

            Node parent = new Node();
            parent.setLabel(analyisiData.get(0).getCaller());
            String caller = analyisiData.get(0).getCaller();
            if(caller != null && !"".equals(caller)) {
                caller = caller.trim();
            }
            parent.setId(Long.parseLong(caller));
            parent.setAnalysis(analyisiData.get(0));


            nodes.add(parent);

//        for (int i = 8887; i > 8830; i--) {
//            Node child = new Node();
//            child.setId(i);
//            child.setLabel(String.valueOf(i));
//
//            nodes.add(child);
//        }


//        for (int i = 8887; i > 8830; i--) {
//            Edge e = new Edge();
//            e.setArrows("from");
//            e.setFrom(123456);
//            e.setTo(i);
//
//            edges.add(e);
//        }


            for (LinkAnalysis la : analyisiData) {
                Node node = new Node();

//            if(la.getCaller().startsWith("880")) {
//                node.setId(Long.parseLong(la.getReceiver().substring(0,la.getCaller().length())));
//            }else {
//                node.setId(Long.parseLong(la.getReceiver()));
//            }
                node.setLabel(la.getReceiver());
                node.setId(Long.parseLong(la.getReceiver()));
                node.setAnalysis(la);

                nodes.add(node);

                Edge edge = new Edge();

                if (la.getType().toUpperCase().contains("MOC")) {

                    edge.setFrom(Long.parseLong(la.getCaller()));
                    edge.setTo(Long.parseLong(la.getReceiver()));
                    edge.setArrows("from");

                } else if (la.getType().toUpperCase().contains("MTC")) {

                    edge.setFrom(Long.parseLong(la.getReceiver()));
                    edge.setTo(Long.parseLong(la.getCaller()));
                    edge.setArrows("from");
                }

                edges.add(edge);
            }

            netwoks.setNodes(nodes);
            netwoks.setEdges(edges);
        }

        return netwoks;
    }

    public Case getCaseDetailsById(int id) {

        return caseRepository.getOne(id);
    }

    public List<CDR> getFrequentCallers(int cid) {

        List<CDR> cdrs = new ArrayList<>();
        List<Suspects> suspects = cdrRepository.suspectedCallsByCid(cid);

        logger.info("MHLOG:: size"+ cid);

//        Collections.sort(suspects, (o1, o2) -> o1.getCount() > o2.getCount() ? suspects.indexOf(o1): suspects.indexOf(o2));


        logger.info("MHLOG:: size suspects "+ suspects.size());

        for (Suspects s: suspects) {
            logger.info("MHLOG:: suspect "+ s);
            List<CDR> sus = cdrRepository.findAllByReceiverAndACaseAndLacAndCellIdOrderByDate(s.getReceiver(),getCaseDetailsById(cid),s.getLac(),s.getCellId());

            logger.info("SUSPECTED "+ sus.size());
            if(sus.size() > 0) {
                cdrs.add(sus.get(0));
            }
        }


        return cdrs;
    }

    public List<IMEIReportDto> getIMEIReport(int cid, String caller) {
        return cdrRepository.getImeiReport(cid,caller);
    }

    public List<CDR> getCDRByAcaseAndReceiverAndIme(int cid, String receiver, String imei) {
        return cdrRepository.findAllByACaseAndReceiverAndImeiOrderByDate(getCaseDetailsById(cid),receiver,imei);
    }

    public List<CDR> getCDRByCallerAndReceiver(int cid, String caller, String receiver) {
        return cdrRepository.findAllByACaseAndCallerAndReceiverOrderByDate(caseRepository.getOne(cid), caller, receiver);
    }

    public List<CDR> getCDRByCallDuration(int caseId) {
        return getCalls(cdrRepository.findAllByACaseAndCallDuration(caseId, 0));
    }

    public List<CDR> getCDRByOutGoingFrequency(int caseId) {
        List<CDR> allByACase = getFrequentCallers(caseId);

        List<CDR> onlyCalls = getCalls(allByACase);

        Iterator<CDR> iter = onlyCalls.iterator();

        while (iter.hasNext()) {
            CDR cdr = iter.next();

            if(cdr.getUsageType().contains("MTC")) {
                iter.remove();
            }
        }

        return onlyCalls;
    }
    public List<CDR> getCDRByIncomingFrequency(int caseId) {
        List<CDR> allByACase = getFrequentCallers(caseId);

        allByACase = getCalls(allByACase);

        Iterator<CDR> iter = allByACase.iterator();

        while (iter.hasNext()) {
            CDR cdr = iter.next();

            if(cdr.getUsageType().contains("MOC")) {
                iter.remove();
            }
        }

        return allByACase;
    }

    public List<AddressDTO> getDistinctCDRByAddress(int caseId) {
        return cdrRepository.getDistinctAddressByACase(caseId);
    }

    public List<CDR> getDetailsByAddress(int caseId, String token) {

        return cdrRepository.findAllByACaseAndAddressOrderByDate(caseRepository.getOne(caseId), token);
    }

    public List<CDR> findAllByDurationAndCase(int caseId) {

        return cdrRepository.findAllByDurationAndCase(caseId);
    }


    public List<AddressDTO> frequentCallAddress(int caseId) {

        return cdrRepository.frequentCallAddress(caseId);
    }

    public List<InternetActivity> storeIP(int caseId, MultipartFile file) {

        Case aCase =  caseRepository.getOne(caseId);

        IPParser parser = new IPParser();

        List<InternetActivityDTO> records = new ArrayList<>();
        List<InternetActivity> iaRecords = new ArrayList<>();
        try {
            parser.parse(file,records);
        }catch (Exception ex) {

        }

        for (InternetActivityDTO iad: records) {
            iaRecords.add(iad.getObject());
        }

        if(iaRecords.size() > 0) {
            for (InternetActivity ia: iaRecords) {
                ia.setACase(aCase);
                internetActivityRepository.save(ia);
            }
        }
        return iaRecords;
    }

    public List<InternetActivity> getInternetActivity(int caseId) {
        return internetActivityRepository.findAllByACase(caseRepository.getOne(caseId));
    }

    public MapItem generateAnalysisMap(int caseId) {
        Netwok analysis = this.getAnalysis(caseId);
        List<Node> nodes = analysis.getNodes();
        List<Edge> edges = analysis.getEdges();

        List<com.retrogradelab.datalize.dto.v2.Node> mNodes = new ArrayList<>();
        List<Connection> connections = new ArrayList<>();

        for (Node node:nodes) {

            com.retrogradelab.datalize.dto.v2.Node n = new com.retrogradelab.datalize.dto.v2.Node();
            n.setText(node.getId()+"");
            n.setUrl(node.getId()+"");
            n.setData(node.getAnalysis().getType());
            mNodes.add(n);

        }

        for (Edge edge:edges) {

            if(edge.getFrom() > 0 && edge.getTo() > 0) {
                Connection con = new Connection();
                con.setTarget(edge.getFrom() + "");
                con.setSource(edge.getTo() + "");

                connections.add(con);
            }

        }

        MapItem mapItem = new MapItem();
        mapItem.setConnections(connections);
        mapItem.setNodes(mNodes);

        return mapItem;
    }
}
