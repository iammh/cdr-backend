package com.retrogradelab.datalize.dto;

import lombok.Data;

@Data
public class Position {

    double lat;
    double lng;
}
