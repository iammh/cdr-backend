package com.retrogradelab.datalize.dto;

import lombok.Data;

@Data
public class LinkAnalysis {

  private int id;
  private String type;
  private String caller;
  private String receiver;

  public LinkAnalysis() {
  }

  public LinkAnalysis(int id,String type, String caller, String receiver) {
    this.id = id;
    this.type = type;
    this.caller = caller;
    this.receiver = receiver;
  }

  public LinkAnalysis(String type, String caller, String receiver) {

    this.type = type;
    this.caller = caller;
    this.receiver = receiver;
  }
}
