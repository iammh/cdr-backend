package com.retrogradelab.datalize.dto.v2;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Node {

    private String text;
    private String url;
    private String data;
    private String color;
    private List<Node> nodes;

    public Node() {
        color = "rgba(128,128,128,0.75)";
        this.nodes = new ArrayList<>();
    }
}
