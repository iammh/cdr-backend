package com.retrogradelab.datalize.dto;


import com.retrogradelab.datalize.entity.InternetActivity;
import lombok.Data;

@Data
public class InternetActivityDTO {

    private String session;

    private String publicIp;
    private String publicPort;
    private String MSISDN;
    private String applicationType;
    private String url;
    private String msIp;
    private String serverIp;


    public InternetActivity getObject() {
        InternetActivity ia = new InternetActivity();
        ia.setSession(session);
        ia.setPublicIp(publicIp);
        ia.setPublicPort(publicPort);
        ia.setMSISDN(MSISDN);
        ia.setApplicationType(applicationType);
        ia.setUrl(url);
        ia.setServerIp(serverIp);
        ia.setMsIp(msIp);

        return ia;
    }
}
