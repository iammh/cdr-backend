package com.retrogradelab.datalize.dto;

import com.retrogradelab.datalize.entity.CDR;
import lombok.Data;

@Data
public class Node {

    private long id;
    private String label;
    private LinkAnalysis analysis;
}
