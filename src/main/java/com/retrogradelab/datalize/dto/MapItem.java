package com.retrogradelab.datalize.dto;

import com.retrogradelab.datalize.dto.v2.Connection;
import com.retrogradelab.datalize.dto.v2.Node;
import lombok.Data;

import java.util.List;

@Data
public class MapItem {

    List<Node> nodes;
    List<Connection> connections;
}
