package com.retrogradelab.datalize.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by shuvo on 3/29/18.
 */
@JsonIgnoreProperties
@Data
public class SearchPayload {

    private String key;
    private String operator;
    private String value;
}
