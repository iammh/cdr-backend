package com.retrogradelab.datalize.dto;


import com.retrogradelab.datalize.entity.CDR;
import lombok.Data;

@Data
public class LocationResponseDto {

    private CDR cdr;
    private Position position;

    public LocationResponseDto(Position position) {
        this.position = position;
    }

    public LocationResponseDto(Position position, CDR cdr) {
        this.position = position;
        this.cdr = cdr;
    }
}
