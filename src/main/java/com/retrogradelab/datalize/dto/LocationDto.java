package com.retrogradelab.datalize.dto;

import lombok.Data;

@Data
public class LocationDto {

  private String address;
  private String thana;
  private String district;
  private String division;
  private String street;
}
