package com.retrogradelab.datalize.dto;

import lombok.Data;

/**
 * Created by shuvo on 5/4/18.
 */

@Data
public class IMEIReportDto {

    private long total;
    private String imei;
    private String receiver;

    public IMEIReportDto(long total, String imei, String receiver) {

        this.total = total;
        this.imei = imei;
        this.receiver = receiver;
    }
}
