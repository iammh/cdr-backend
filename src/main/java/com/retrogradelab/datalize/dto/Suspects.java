package com.retrogradelab.datalize.dto;


import lombok.Data;

@Data
public class Suspects {

    private long count;
    private String receiver;
    private String cellId;
    private String lac;

    public Suspects(long count, String receiver, String cellId, String lac) {
        this.count = count;
        this.receiver = receiver;
        this.cellId = cellId;
        this.lac = lac;
    }

    public Suspects(long count, String receiver) {
        this.count = count;
        this.receiver = receiver;
        this.cellId = cellId;
        this.lac = lac;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }
}
