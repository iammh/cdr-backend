package com.retrogradelab.datalize.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.helper.CustomerDateAndTimeDeserialize;
import lombok.Data;

import java.util.Date;

@Data
public class Record {

    @JsonDeserialize(using = CustomerDateAndTimeDeserialize.class)
    private Date date;
    private String caller;
    private String receiver;
    private double duration;
    private String usageType;
    private String lac;
    private String cell;
    private String newCell;
    private String imei;
    private String address;

    public CDR buildCDR() {
        CDR cdr = new CDR();
        cdr.setDate(this.date);
        cdr.setCaller(this.caller);
        cdr.setReceiver(this.receiver);
        cdr.setCallDuration(this.duration);
        cdr.setUsageType(this.usageType);
        cdr.setLac(this.lac);
        cdr.setCellId(this.cell);
        cdr.setImei(this.imei);
        cdr.setAddress(this.address);
        return cdr;
    }
}
