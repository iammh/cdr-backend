package com.retrogradelab.datalize.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.retrogradelab.datalize.helper.CustomerDateAndTimeDeserialize;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaseDto {

    private String name;

    @JsonDeserialize(using = CustomerDateAndTimeDeserialize.class)
    private Date caseDate;

    @JsonDeserialize(using = CustomerDateAndTimeDeserialize.class)
    private Date fileDate;

    @JsonProperty("assignee")
    private String asignee;

    @JsonProperty("investigationOfficier")
    private String investigator;
}
