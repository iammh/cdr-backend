package com.retrogradelab.datalize.dto;

import com.retrogradelab.datalize.entity.CELLID;
import com.retrogradelab.datalize.entity.Location;
import lombok.Data;

@Data
public class CellIdRecord {

  private String sector;
  private String cellId;
  private String lac;
  private double latitude;
  private double longitude;
  private String siteName;
  private int orientation;
  private String cgi;
  private String typeOfRbs;
  private String address;
  private String street;
  private String thana;
  private String division;
  private String district;


  public CELLID build() {
    CELLID cell = new CELLID();

    cell.setSector(this.sector);
    cell.setCellId(this.cellId);
    cell.setLac(this.lac);
    cell.setLatitude(this.latitude);
    cell.setLongitude(this.longitude);
    cell.setSiteName(this.siteName);
    cell.setOrientation(this.orientation);
    cell.setCgi(this.cgi);
    cell.setTypeOfRbs(this.typeOfRbs);

    Location location = new Location();
    location.setAddress(this.address);
    location.setDistrict(this.district);
    location.setThana(this.thana);
    location.setDivision(this.division);
    location.setStreet(this.street);

    cell.setLocation(location);

    return cell;
  }
}
