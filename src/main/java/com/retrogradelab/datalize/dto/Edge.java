package com.retrogradelab.datalize.dto;

import lombok.Data;

@Data
public class Edge {

    private long from;
    private long to;
    private String arrows;
}
