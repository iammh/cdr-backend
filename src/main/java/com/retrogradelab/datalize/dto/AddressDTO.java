package com.retrogradelab.datalize.dto;


import lombok.Data;

@Data
public class AddressDTO {

  private long count;
  private String caller;
  private String receiver;

  private String address;
  private String action = "";

  public AddressDTO(String address) {
    this.address = address;
  }

  public AddressDTO(long count, String caller, String receiver, String address) {
    this.count = count;
    this.caller = caller;
    this.receiver = receiver;
    this.address = address;
  }
}
