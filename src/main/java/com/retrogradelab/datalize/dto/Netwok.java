package com.retrogradelab.datalize.dto;

import lombok.Data;

import java.util.List;

@Data
public class Netwok {

    private List<Node> nodes;
    private List<Edge> edges;
}
