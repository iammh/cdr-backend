package com.retrogradelab.datalize.dao;

import com.retrogradelab.datalize.dto.SearchPayload;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by shuvo on 3/30/18.
 */
@Repository
public class CDRDaoImpl implements CDRDao {

    private final static Logger logger = LoggerFactory.getLogger(CDRDaoImpl.class);


    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CDR> searchByCriteria(List<SearchPayload> payloads) {

        logger.info("PAYLOADS ", payloads.getClass());
        logger.info("PAYLOADS HashMap ", payloads.hashCode());

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CDR> query = builder.createQuery(CDR.class);
        Root r = query.from(CDR.class);

        Predicate predicate = builder.conjunction();

        for (SearchPayload payload:payloads) {

            if(payload.getKey().toUpperCase().contains("DATE")) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

                Date date = null;
                try {
                    date = sdf.parse(payload.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                logger.info("MHLOG:: date "+ date);
                if (payload.getOperator().equals("eq")) {
                    predicate = builder.and(predicate,
                            builder.equal(r.get("date"), date));
                }else if(payload.getOperator().equals("gte")) {
                    predicate = builder.and(predicate,
                            builder.greaterThanOrEqualTo(r.get("date"), date));
                } else if(payload.getOperator().equals("gt")) {
                    predicate = builder.and(predicate,
                            builder.greaterThan(r.get("date"), date));
                } else if(payload.getOperator().equals("lte")) {
                    predicate = builder.and(predicate,
                            builder.lessThanOrEqualTo(r.get("date"), date));
                } else if(payload.getOperator().equals("lt")) {
                    predicate = builder.and(predicate,
                            builder.lessThan(r.get("date"), date));
                }
            }else {
                if (payload.getOperator().equals("eq")) {
                    predicate = builder.and(predicate,
                            builder.equal(r.get(payload.getKey()), payload.getValue()));
                }
            }
        }


        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List suspectedCallsByCid(int caseId) {
        Query query = entityManager.createQuery("select count(id) as calls, receiver from CDR where length(receiver) > 6 and a_case_id = ? group by receiver,id order by calls desc");
        query.setParameter(0, caseId);
//        query.getHints();
        logger.info("MHLOG:: "+ query.getHints().toString());


        return query.getResultList();
    }

}
