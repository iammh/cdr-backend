package com.retrogradelab.datalize.dao;

import com.retrogradelab.datalize.dto.SearchPayload;
import com.retrogradelab.datalize.entity.CDR;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * Created by shuvo on 3/30/18.
 */
public interface CDRDao {

    List<CDR> searchByCriteria(List<SearchPayload> payloads);
    List suspectedCallsByCid(int caseId);
}
