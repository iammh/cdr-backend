package com.retrogradelab.datalize.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebController {

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String index() {
    return "index";
  }

  @RequestMapping(value = "/graph", method = RequestMethod.GET)
  public String graph(@RequestParam(value = "caseId") int caseId, @RequestParam("w") int w, @RequestParam("h") int h, Model uiModel) {

    uiModel.addAttribute("caseId", caseId);
    uiModel.addAttribute("width", w);
    uiModel.addAttribute("height", h);

    return "hello";
  }
}
