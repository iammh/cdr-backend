package com.retrogradelab.datalize.controller;

import com.retrogradelab.datalize.builder.CaseBuilder;
import com.retrogradelab.datalize.builder.ColumnBuilder;
import com.retrogradelab.datalize.constants.*;
import com.retrogradelab.datalize.dto.*;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.entity.CELLID;
import com.retrogradelab.datalize.entity.Case;
import com.retrogradelab.datalize.entity.InternetActivity;
import com.retrogradelab.datalize.processor.Parser;
import com.retrogradelab.datalize.service.CaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/cdr")
public class AnalyzerController {

    private final static Logger log = LoggerFactory.getLogger(AnalyzerController.class);

    @Autowired
    private CaseService caseService;

    //Get all the cases
    @GetMapping(value = "/cases")
    public List<Case> index() {

        return caseService.findAll();
    }

    @PostMapping(value = "/cases/create")
    public Case createCase(HttpServletRequest request, @RequestBody CaseDto caseDto) {

        return caseService.create(CaseBuilder.build(caseDto));
    }

    @PostMapping(value = "/{_case}/upload")
    public List<Record> createCDR(@RequestParam("file")MultipartFile file, @PathVariable("_case") int id, @RequestParam("operator") int operator) throws Exception{

        List<Record> records = new ArrayList<Record>();


        ColumnIndex columnIndex = null;
        if(operator == 1) {
            columnIndex = ColumnBuilder.build(Types.AIRTEL);
        }
        else if(operator == 2) {
            columnIndex = ColumnBuilder.build(Types.BANGLALINK);
        }else if(operator == 3) {
            columnIndex = ColumnBuilder.build(Types.GRAMEENPHONE);
        }

        Parser.parse(file, records, columnIndex);

        if(records.size() > 0) {
            caseService.storeCRD(id, file.getOriginalFilename(), file.getContentType(),records);
        }

        return records;
    }

    @GetMapping(value = "/calldetails")
    public List<CDR> listCDR(@RequestParam("cid") int caseId) {
        return caseService.getCRDRecordsByCase(caseId);
    }

    @PostMapping(value = "/{cid}/store/cell")
    public List<CELLID> storeCellId(@PathVariable("cid") int cid, @RequestParam(value = "operator") int operator, @RequestParam("file") MultipartFile file) {
        return caseService.storeCell(cid, operator, file);
    }

    @GetMapping(value = "/celldetails")
    public List<CELLID> getCellId(@RequestParam("cid") int cid) {
        return caseService.getCellId(cid);
    }

    @GetMapping(value = "/cases/{cid}/nightcalls")
    public List<CDR> nightCalls(@PathVariable("cid") int cid) {



        return caseService.getNightCallsByCid(cid);
    }

    @GetMapping(value = "/cases/{cid}/timedcalls")
    public List<CDR> timedCalls(@PathVariable("cid") int cid, @RequestParam("time") String time) {

        return caseService.getTimedCallsByCid(cid,time);
    }

    @GetMapping(value = "/cases/{cid}/suspects")
    public List<CDR> suspects(@PathVariable("cid") int cid) {

        Suspects sus = caseService.getSuspectedCallsByCid(cid);

        log.info("MHLOG:: "+ sus);

        return caseService.getSuspects(cid,sus.getLac(), sus.getCellId(), sus.getReceiver());
    }

    @GetMapping(value = "/cases/{cid}/callfrequency")
    public List<Suspects> callFrequency(@PathVariable("cid") int cid) {


        return caseService.getSuspectedCallsFrequencyByCase(cid);
    }

    @GetMapping(value = "/cases/{cid}/locationbycell")
    public Set<LocationResponseDto> findAllByACaseAndGroupByCellId(@PathVariable("cid") int caseId) {

        List<CDR> locationsData = caseService.findAllByACaseAndGroupByCellId(caseId);

        Set<LocationResponseDto> locationResponseDtoList = new HashSet<>();

        for (CDR cdr: locationsData) {

            Position position = new Position();

            position.setLng(cdr.getLongitude());
            position.setLat(cdr.getLatitude());

            LocationResponseDto lrd = new LocationResponseDto(position,cdr);
            locationResponseDtoList.add(lrd);
        }

        return locationResponseDtoList;
    }

    @GetMapping(value = "/cases/{cid}/analysis")
    public Netwok analyses(@PathVariable("cid") int caseId) {
        return caseService.getAnalysis(caseId);
    }

    @GetMapping(value = "/cases/details")
    public Case getCaseDetailsById(@RequestParam("id") int id) {
        return caseService.getCaseDetailsById(id);
    }


    @GetMapping(value = "/cases/{cid}/fcallers")
    public List<CDR> getFrequentCaller(@PathVariable("cid") int cid) {
        return caseService.getFrequentCallers(cid);
    }

    @GetMapping(value = "/cases/{cid}/imeireport")
    public List<IMEIReportDto> getIMEIReport(@PathVariable("cid") int cid, @RequestParam("caller") String caller) {
        return caseService.getIMEIReport(cid,caller);
    }

    @GetMapping(value = "/cases/{cid}/imei/details")
    public List<CDR> getCDRByImeiAndReceiver(@PathVariable("cid") int cid, @RequestParam("receiver") String reveiver, @RequestParam("imei") String imei) {
        return caseService.getCDRByAcaseAndReceiverAndIme(cid,reveiver,imei);
    }

    @GetMapping(value = "/cases/{cid}/cdr/byreceiverandcaller")
    public List<CDR> getCDRByCallerAndReceiver(@PathVariable("cid") int cid, @RequestParam("caller") String caller, @RequestParam("receiver") String receiver) {
        return caseService.getCDRByCallerAndReceiver(cid,caller,receiver);
    }

    @GetMapping(value = "/cases/case/details")
    public Case getDetailsCase(@RequestParam("id") int caseId) {
        return caseService.getCaseDetailsById(caseId);
    }

    @GetMapping(value = "/cases/silentcalls")
    public List<CDR> getSilentCallCDR(@RequestParam("id") int caseId) {
        return caseService.getCDRByCallDuration(caseId);
    }

    @GetMapping(value = "/cases/outgoingfrequency")
    public List<CDR> getCDRByOutGoingFrequency(@RequestParam("id") int id) {
        return caseService.getCDRByOutGoingFrequency(id);
    }


    @GetMapping(value = "/cases/incomingfrequency")
    public List<CDR> getCDRByIncomingFrequency(@RequestParam("id") int id) {
        return caseService.getCDRByIncomingFrequency(id);
    }

    @GetMapping(value = "/cases/distinct/address")
    public List<AddressDTO> getDistinctCDRByAddress(@RequestParam("id") int id) {
        return caseService.getDistinctCDRByAddress(id);
    }

    @GetMapping(value = "/cases/{cid}/detailsbyaddress")
    public List<CDR> getDetailsByAddress(@RequestParam("token") String token, @PathVariable("cid") int caseId) {
        return caseService.getDetailsByAddress(caseId, token);
    }

    @GetMapping(value = "/cases/{cid}/callduration")
    public List<CDR> findAllByDurationAndCase(@PathVariable("cid") int caseId) {
        return caseService.findAllByDurationAndCase(caseId);
    }

    @GetMapping(value = "/cases/{cid}/frequentcalladdress")
    public List<AddressDTO> frequentCallAddress(@PathVariable("cid") int caseId) {
        return caseService.frequentCallAddress(caseId);
    }

    @PostMapping(value = "/{cid}/store/ip")
    public List<InternetActivity> storeIP(@PathVariable("cid") int caseId, @RequestParam("file") MultipartFile file) {
        return caseService.storeIP(caseId, file);
    }

    @GetMapping(value = "/cases/{cid}/ip/bycase")
    public List<InternetActivity> getIpDetails(@PathVariable("cid") int caseId) {
        return caseService.getInternetActivity(caseId);
    }

    @GetMapping(value = "/cases/{cid}/analysis-map")
    public MapItem getAnalysisMap(@PathVariable("cid") int caseId) {
        return caseService.generateAnalysisMap(caseId);
    }
}
