package com.retrogradelab.datalize.controller;

import com.retrogradelab.datalize.dao.CDRDao;
import com.retrogradelab.datalize.dao.CDRDaoImpl;
import com.retrogradelab.datalize.dto.SearchPayload;
import com.retrogradelab.datalize.entity.CDR;
import com.retrogradelab.datalize.repository.CDRRepository;
import com.retrogradelab.datalize.service.CaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuvo on 3/29/18.
 */
@RestController
public class SearchController {

    @Autowired
    private CaseService caseService;

    private final static Logger logger = LoggerFactory.getLogger(SearchController.class);

    @RequestMapping(value = "/cdr/search/cdr", method = RequestMethod.POST)
    public List<CDR> searchCDR(@RequestBody List<SearchPayload> payloads) {

//        logger.info("CONTROLLER Payloads "+ payloads);
//
//        List<SearchPayload> loads = new ArrayList<>();
//
//        for (SearchPayload payload:payloads) {
//            SearchPayload sl = new SearchPayload();
//            sl.setKey(payload.getKey());
//            sl.setOperator(payload.getOperator());
//            sl.setValue(payload.getOperator());
//
//            loads.add(sl);
//        }
//
        logger.info("Before sending to the dao "+ payloads);


        return caseService.searchByCriteria(payloads);
    }

}
