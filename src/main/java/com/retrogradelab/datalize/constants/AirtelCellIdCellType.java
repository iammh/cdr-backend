package com.retrogradelab.datalize.constants;

public enum AirtelCellIdCellType {

  SITE_NAME(0),
  SECTOR(1),
  CI(2),
  LAC(3),
  CGI(4),
  ORIENTATION(5),
  LAT(6),
  LANG(7),
  RBS_TYPE(8),
  DISTRICT(9),
  THANA(10),
  ADDRESS(11);

  private final int index;
  private AirtelCellIdCellType(int index) {
    this.index = index;
  }


  public int getIndex() {
    return index;
  }
}
