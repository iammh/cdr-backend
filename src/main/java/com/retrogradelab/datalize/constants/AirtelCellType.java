package com.retrogradelab.datalize.constants;

public enum AirtelCellType{
    DATE(0),
    CALLER(1),
    RECEIVER(2),
    DURATION(3),
    USAGE_TYPE(4),
    LAC(5),
    CELL(6),
    NEWCELL(7),
    IMEI(8),
    ADDRESS(9);

    private final int index;
    private AirtelCellType(int index) {
        this.index = index;
    }


    public int getIndex() {
        return index;
    }



}
