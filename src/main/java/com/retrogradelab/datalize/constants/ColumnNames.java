package com.retrogradelab.datalize.constants;

public enum ColumnNames {

    DATE("date"),
    CALLER("caller"),
    RECEIVER("receiver"),
    DURATION("duration"),
    USAGE_TYPE("usageType"),
    LAC("lac"),
    CELL("cell"),
    IMEI("imei"),
    ADDRESS("address"),
    LATITUDE("lat"),
    LONGITUDE("lang");

    private String name;
    private ColumnNames(String name) {
        this.name = name;
    }

}
