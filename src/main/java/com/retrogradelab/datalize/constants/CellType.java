package com.retrogradelab.datalize.constants;

public enum  CellType {

    DEFFAULT(-1);

    private final int index;
    private CellType(int index) {
        this.index = index;
    }


    public int getIndex() {
        return index;
    }

}
