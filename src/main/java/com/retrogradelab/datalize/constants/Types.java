package com.retrogradelab.datalize.constants;

public enum Types {
    AIRTEL,
    GRAMEENPHONE,
    ROBI,
    BANGLALINK,
    TELETALK,

    AIRTEL_CELL,
    GP_CELL,
    ROBI_CELL,
    BANGLALINK_CELL,
    TELETALK_CELL;
}
