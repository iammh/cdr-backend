package com.retrogradelab.datalize.constants;

public enum BanglaLinkCellType {

    DATE(0),
    PROVIDER(1),
    CALLER(2),
    RECEIVER(3),
    DURATION(4),
    USAGE_TYPE(5),
    LAC(6),
    CELL(7),
    IMEI(8),
    ADDRESS(9);

    private final int index;
    private BanglaLinkCellType(int index) {
        this.index = index;
    }


    public int getIndex() {
        return index;
    }
}
