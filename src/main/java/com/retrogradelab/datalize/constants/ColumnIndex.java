package com.retrogradelab.datalize.constants;

import java.util.Map;

public abstract class ColumnIndex {

    protected Map<String, Integer> columns;

    public ColumnIndex(Map<String, Integer> columns) {
        this.columns = columns;
    }

    public int getIndex(String key) {
        return this.columns.get(key).intValue();
    }

    public Map<String, Integer> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, Integer> columns) {
        this.columns = columns;
    }
}
