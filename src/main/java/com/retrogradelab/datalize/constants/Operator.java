package com.retrogradelab.datalize.constants;

public enum  Operator {
    AIRTEL(1),
    GRAMEEN(2),
    BANGLALINK(3),
    TELETALK(4);

    private int code;
    private Operator(int code) {
        this.code = code;
    }
}
