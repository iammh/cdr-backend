package com.retrogradelab.datalize.processor;

import com.retrogradelab.datalize.dto.InternetActivityDTO;
import com.retrogradelab.datalize.entity.InternetActivity;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

public class IPParser {

    public void parse(MultipartFile file, List<InternetActivityDTO> records) throws Exception {
        InputStream data = file.getInputStream();

        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));

        CSVParser parser = new CSVParser(reader,CSVFormat.DEFAULT);

        for (CSVRecord record: parser.getRecords()) {
            InternetActivityDTO dto = new InternetActivityDTO();
            dto.setSession(record.get(0));
            dto.setPublicIp(record.get(2));
            dto.setPublicPort(record.get(3));
            dto.setMSISDN(record.get(9));
            dto.setServerIp(record.get(12));
            dto.setUrl(record.get(22));
            dto.setMsIp(record.get(11));

            records.add(dto);
        }


    }
}
