package com.retrogradelab.datalize.processor;

import com.retrogradelab.datalize.constants.ColumnIndex;
import com.retrogradelab.datalize.constants.ColumnNames;
import com.retrogradelab.datalize.dto.Record;
import com.retrogradelab.datalize.utils.Util;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class Parser {



    public static void parse(MultipartFile file, List<Record> records, ColumnIndex columnIndex) throws Exception{

        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if(extension != null && !"".equals(extension) && extension.equals("xlsx")) {
            XLSXParser(file, records, columnIndex);
        }else if(extension != null && !"".equals(extension) && extension.equals("xls")) {
            XLSParser(file, records, columnIndex);
        }else {
            throw new Exception("Could not parse document");
        }

    }

    private static void XLSXParser(MultipartFile file, List<Record> records, ColumnIndex columnIndex) throws Exception{

        InputStream ExcelFileToRead = file.getInputStream();
        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

        int totalSheets = wb.getNumberOfSheets();
        for (int index=0;index< totalSheets;index++) {
            XSSFSheet sheet=wb.getSheetAt(index);
            XSSFRow row;

            Iterator rows = sheet.rowIterator();

            while (rows.hasNext()) {
                row = (XSSFRow) rows.next();
                if(row.getCell(0).getCellTypeEnum().equals(CellType.STRING)) {
                    if(row.getCell(0).getStringCellValue().toUpperCase().contains("DATE") && row.getCell(0).getStringCellValue().toUpperCase().contains("TIME")) {
                        break;
                    }
                }
            }


            while (rows.hasNext())
            {
                row=(XSSFRow) rows.next();
                Iterator cells = row.cellIterator();
                if(row.getCell(0)== null || row.getCell(0).getCellTypeEnum().equals(CellType.BLANK)) {
                    continue;
                }

                Record record = new Record();

                //set record Date
                XSSFCell cellDate = row.getCell(columnIndex.getIndex(ColumnNames.DATE.name()));

                if(cellDate.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setDate(Util.strToDate(cellDate.getStringCellValue()));
                }else if(cellDate.getCellTypeEnum().equals(CellType.NUMERIC)){
                    record.setDate(cellDate.getDateCellValue());
                }

                //set caller
                XSSFCell cellCaller = row.getCell(columnIndex.getIndex(ColumnNames.CALLER.name()));
                if(cellCaller.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setCaller(cellCaller.getStringCellValue());
                }else if(cellCaller.getCellTypeEnum().equals(CellType.NUMERIC)){
                    record.setCaller(Util.doubleToString(cellCaller.getNumericCellValue()));
                }

                //set receiver
                XSSFCell cellReceiver = row.getCell(columnIndex.getIndex(ColumnNames.RECEIVER.name()));
                if(cellReceiver.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setReceiver(cellReceiver.getStringCellValue());
                }else if(cellReceiver.getCellTypeEnum().equals(CellType.NUMERIC)){
                    record.setReceiver(Util.doubleToString(cellReceiver.getNumericCellValue()));
                }

                //set duration
                XSSFCell cellDuration = row.getCell(columnIndex.getIndex(ColumnNames.DURATION.name()));
                if(cellDuration.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setDuration(Double.parseDouble(cellDuration.getStringCellValue()));
                }else if(cellDuration.getCellTypeEnum().equals(CellType.NUMERIC)){
                    record.setDuration(cellDuration.getNumericCellValue());
                }

                //set usageType
                XSSFCell cellUsageType = row.getCell(columnIndex.getIndex(ColumnNames.USAGE_TYPE.name()));
                if(cellUsageType.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setUsageType(cellUsageType.getStringCellValue());
                }

                //set lac
                XSSFCell cellLac = row.getCell(columnIndex.getIndex(ColumnNames.LAC.name()));
                if(cellLac.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setLac(cellLac.getStringCellValue());
                }else if(cellLac.getCellTypeEnum().equals(CellType.NUMERIC)) {
                    record.setLac(Util.doubleToString(cellLac.getNumericCellValue()));
                }

                //set CELLID
                XSSFCell cellCellId = row.getCell(columnIndex.getIndex(ColumnNames.CELL.name()));
                if(cellCellId.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setCell(cellCellId.getStringCellValue());
                }else if(cellCellId.getCellTypeEnum().equals(CellType.NUMERIC)) {
                    record.setCell(Util.doubleToString(cellCellId.getNumericCellValue()));
                }

                //set IMEI
                XSSFCell cellIMEI = row.getCell(columnIndex.getIndex(ColumnNames.IMEI.name()));
                if(cellIMEI.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setImei(cellIMEI.getStringCellValue());
                }else if(cellIMEI.getCellTypeEnum().equals(CellType.NUMERIC)) {
                    record.setImei(Util.doubleToString(cellIMEI.getNumericCellValue()));
                }

                //set ADDRESS
                XSSFCell cellAddress = row.getCell(columnIndex.getIndex(ColumnNames.ADDRESS.name()));
                if(cellAddress.getCellTypeEnum().equals(CellType.STRING)) {
                    record.setAddress(cellAddress.getStringCellValue());
                }


                records.add(record);

            }
        }

    }

    private static void XLSParser(MultipartFile file, List<Record> records, ColumnIndex columnIndex) throws Exception{
        InputStream ExcelFileToRead = file.getInputStream();
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        int totalSheets = wb.getNumberOfSheets();
        for (int index=0;index< totalSheets;index++) {
            HSSFSheet sheet=wb.getSheetAt(index);
            HSSFRow row;

            Iterator rows = sheet.rowIterator();
            try {
                while (rows.hasNext()) {
                    row = (HSSFRow) rows.next();
                    if(row.getCell(0).getCellTypeEnum().equals(CellType.STRING)) {
                        if(row.getCell(0).getStringCellValue().toUpperCase().contains("DATE") || row.getCell(0).getStringCellValue().toUpperCase().contains("TIME") || row.getCell(0).getStringCellValue().toUpperCase().contains("START")) {
                            break;
                        }
                    }
                }
            }catch (Exception ex){}


            while (rows.hasNext())
            {
                try {


                    row=(HSSFRow) rows.next();
                    Iterator cells = row.cellIterator();
                    if(row.getCell(0)== null || row.getCell(0).getCellTypeEnum().equals(CellType.BLANK)) {
                        continue;
                    }

                    Record record = new Record();

                    //set record Date
                    try{
                        HSSFCell cellDate = row.getCell(columnIndex.getIndex(ColumnNames.DATE.name()));

                        if(cellDate.getCellTypeEnum().equals(CellType.STRING)) {
                            record.setDate(Util.strToDate(cellDate.getStringCellValue()));
                        }else if(cellDate.getCellTypeEnum().equals(CellType.NUMERIC)){
                            record.setDate(Util.strToDate(cellDate.getNumericCellValue()+""));
//                            record.setDate(null);
                        }
                    }catch (Exception ex) {
                        record.setDate(null);
                    }

                    //set caller
                    HSSFCell cellCaller = row.getCell(columnIndex.getIndex(ColumnNames.CALLER.name()));
                    if(cellCaller.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setCaller(cellCaller.getStringCellValue());
                    }else if(cellCaller.getCellTypeEnum().equals(CellType.NUMERIC)){
                        record.setCaller(Util.doubleToString(cellCaller.getNumericCellValue()));
                    }

                    //set receiver
                    HSSFCell cellReceiver = row.getCell(columnIndex.getIndex(ColumnNames.RECEIVER.name()));
                    if(cellReceiver.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setReceiver(cellReceiver.getStringCellValue());
                    }else if(cellReceiver.getCellTypeEnum().equals(CellType.NUMERIC)){
                        record.setReceiver(Util.doubleToString(cellReceiver.getNumericCellValue()));
                    }

                    //set duration
                    HSSFCell cellDuration = row.getCell(columnIndex.getIndex(ColumnNames.DURATION.name()));
                    if(cellDuration.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setDuration(Double.parseDouble(cellDuration.getStringCellValue()));
                    }else if(cellDuration.getCellTypeEnum().equals(CellType.NUMERIC)){
                        record.setDuration(cellDuration.getNumericCellValue());
                    }

                    //set usageType
                    HSSFCell cellUsageType = row.getCell(columnIndex.getIndex(ColumnNames.USAGE_TYPE.name()));
                    if(cellUsageType.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setUsageType(cellUsageType.getStringCellValue());
                    }

                    //set lac
                    HSSFCell cellLac = row.getCell(columnIndex.getIndex(ColumnNames.LAC.name()));
                    if(cellLac.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setLac(cellLac.getStringCellValue());
                    }else if(cellLac.getCellTypeEnum().equals(CellType.NUMERIC)) {
                        record.setLac(Util.doubleToString(cellLac.getNumericCellValue()));
                    }

                    //set CELLID
                    HSSFCell cellCellId = row.getCell(columnIndex.getIndex(ColumnNames.CELL.name()));
                    if(cellCellId.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setCell(cellCellId.getStringCellValue());
                    }else if(cellCellId.getCellTypeEnum().equals(CellType.NUMERIC)) {
                        record.setCell(Util.doubleToString(cellCellId.getNumericCellValue()));
                    }

                    //set IMEI
                    HSSFCell cellIMEI = row.getCell(columnIndex.getIndex(ColumnNames.IMEI.name()));
                    if(cellIMEI.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setImei(cellIMEI.getStringCellValue());
                    }else if(cellIMEI.getCellTypeEnum().equals(CellType.NUMERIC)) {
                        record.setImei(Util.doubleToString(cellIMEI.getNumericCellValue()));
                    }

                    //set ADDRESS
                    HSSFCell cellAddress = row.getCell(columnIndex.getIndex(ColumnNames.ADDRESS.name()));
                    if(cellAddress.getCellTypeEnum().equals(CellType.STRING)) {
                        record.setAddress(cellAddress.getStringCellValue());
                    }


                    records.add(record);

                } catch (Exception ex) {

                }

            }
        }
    }

}
