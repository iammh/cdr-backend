package com.retrogradelab.datalize.processor;

import com.retrogradelab.datalize.dto.CellIdRecord;
import com.retrogradelab.datalize.entity.CELLID;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class CellParser {

  public void parse(MultipartFile file, List<CellIdRecord> records) throws Exception {
    InputStream data = file.getInputStream();

    String extension = FilenameUtils.getExtension(file.getOriginalFilename());
    if(extension != null && !"".equals(extension) && extension.equals("xlsx")) {
      XLSXParser(data, records);
    }else if(extension != null && !"".equals(extension) && extension.equals("xls")) {
      XLSParser(data, records);
    }else {
      throw new Exception("Could not parse document");
    }

  }

  private static void XLSXParser(InputStream ExcelFileToRead, List<CellIdRecord> records) throws Exception {

    XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

    int totalSheets = wb.getNumberOfSheets();
    for (int index = 0; index < totalSheets; index++) {

      XSSFSheet sheet = wb.getSheetAt(index);

      XSSFRow row;

      Iterator rows = sheet.rowIterator();

      if(rows.hasNext()) {
        rows.next();
      }

      while (rows.hasNext()) {
        row = (XSSFRow) rows.next();

        try {
          if(row.getCell(0)== null || row.getCell(0).getCellTypeEnum().equals(CellType.BLANK)) {
            continue;
          }

          CellIdRecord record = new CellIdRecord();

          XSSFCell cell = row.getCell(0);

          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setSiteName(cell.getStringCellValue());
          }

          cell = row.getCell(1);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setSector(cell.getStringCellValue());
          }
          cell = row.getCell(2);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setCellId(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setCellId(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(3);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLac(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLac(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(4);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setCgi(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setCgi(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(5);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setOrientation((int) Double.parseDouble(cell.getStringCellValue()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setOrientation((int)cell.getNumericCellValue());
          }

          cell = row.getCell(6);

          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLongitude(Double.parseDouble(cell.getStringCellValue().trim()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLongitude(cell.getNumericCellValue());
          }


          cell = row.getCell(7);
          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLatitude(Double.parseDouble(cell.getStringCellValue().trim()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLatitude(cell.getNumericCellValue());
          }

          cell = row.getCell(8);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setTypeOfRbs(cell.getStringCellValue());
          }

          cell = row.getCell(9);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setDistrict(cell.getStringCellValue());
          }
          cell = row.getCell(10);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setThana(cell.getStringCellValue());
          }
          cell = row.getCell(11);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setAddress(cell.getStringCellValue());
          }

          records.add(record);
        }catch (Exception ex) {}


      }


    }


  }

  private static void XLSParser(InputStream ExcelFileToRead, List<CellIdRecord> records) throws Exception {

    HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);


    int totalSheets = wb.getNumberOfSheets();

    for (int index = 0; index < totalSheets; index++) {

      HSSFSheet sheet = wb.getSheetAt(index);

      HSSFRow row;

      Iterator rows = sheet.rowIterator();

      if(rows.hasNext()) {
        rows.next();
      }

      while (rows.hasNext()) {
        row = (HSSFRow) rows.next();

        try {
          if(row.getCell(0)== null || row.getCell(0).getCellTypeEnum().equals(CellType.BLANK)) {
            continue;
          }

          CellIdRecord record = new CellIdRecord();

          HSSFCell cell = row.getCell(0);

          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setSiteName(cell.getStringCellValue());
          }

          cell = row.getCell(1);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setSector(cell.getStringCellValue());
          }
          cell = row.getCell(2);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setCellId(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setCellId(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(3);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLac(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLac(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(4);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setCgi(cell.getStringCellValue());
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setCgi(cell.getNumericCellValue()+"");
          }
          cell = row.getCell(5);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setOrientation((int) Double.parseDouble(cell.getStringCellValue()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setOrientation((int)cell.getNumericCellValue());
          }

          cell = row.getCell(6);

          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLongitude(Double.parseDouble(cell.getStringCellValue().trim()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLongitude(cell.getNumericCellValue());
          }


          cell = row.getCell(7);
          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setLatitude(Double.parseDouble(cell.getStringCellValue().trim()));
          }else if(cell.getCellTypeEnum().equals(CellType.NUMERIC)) {
            record.setLatitude(cell.getNumericCellValue());
          }

          cell = row.getCell(8);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setTypeOfRbs(cell.getStringCellValue());
          }

          cell = row.getCell(9);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setDistrict(cell.getStringCellValue());
          }
          cell = row.getCell(10);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setThana(cell.getStringCellValue());
          }
          cell = row.getCell(11);


          if(cell.getCellTypeEnum().equals(CellType.STRING)) {
            record.setAddress(cell.getStringCellValue());
          }

          records.add(record);
        }catch (Exception ex) {}


      }


    }


  }
}
