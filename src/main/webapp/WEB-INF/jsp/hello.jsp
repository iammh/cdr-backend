<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

    <!-- Access the bootstrap Css like this,
        Spring boot will handle the resource mapping automcatically -->
    <%--<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />--%>

    <!--
	<%--<spring:url value="/css/main.css" var="springCss" />--%>
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <%--<c:url value="/css/main.css" var="jstlCss" />--%>
    <%--<link href="${jstlCss}" rel="stylesheet" />--%>

    <script type="text/javascript" src="css/jquery.min.js"></script>
    <script type="text/javascript" src="css/raphael.min.js"></script>
    <script type="text/javascript" src="css/main.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/graphdracula/1.0.3/dracula.min.js"></script>


    <script type="text/javascript">
        redraw = function(layouter) {
            layouter.layout();
            renderer.draw();
        };
        window.onload = function() {

            var g = new Graph();

            $.get(window.location.origin+'/datalize/cdr/calldetails?cid=${caseId}', function (data) {


                var sailNames = data;
                $.each(sailNames, function (i,item) {
                    // console.log("Node added ", item);
                    g.addEdge(item.caller, item.receiver);
                });
                /* layout the graph using the Spring layout implementation */
                var layouter = new Graph.Layout.Spring(g);
                layouter.layout();

                /* draw the graph using the RaphaelJS draw implementation */
                var renderer = new Graph.Renderer.Raphael('canvas', g, ${width},${height});
                renderer.draw(layouter);


              })





        };
    </script>
</head>
<body>


    <div id="canvas"></div>


</body>

</html>