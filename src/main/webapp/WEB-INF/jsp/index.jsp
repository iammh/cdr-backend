<!-- <!DOCTYPE html>
<html lang="en">
    <head><meta charset="utf-8"
        ><meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="/favicon.ico"><title>datalize-ui</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="js/about.0aedd84b.js" rel="prefetch">
        <link href="css/app.5eebaa78.css" rel="preload" as="style">
        <link href="css/chunk-vendors.d12242eb.css" rel="preload" as="style">
        <link href="js/app.72ed0247.js" rel="preload" as="script">
        <link href="js/chunk-vendors.48a09eaf.js" rel="preload" as="script">
        <link href="css/chunk-vendors.d12242eb.css" rel="stylesheet">
        <link href="css/app.5eebaa78.css" rel="stylesheet">
    </head>
    <body id="app">
        <noscript><strong>We're sorry but datalize-ui doesn't work properly without JavaScript enabled. Please enable it to continue.

        </strong></noscript>
        <script src="js/chunk-vendors.48a09eaf.js"></script>
        <script src="js/app.72ed0247.js"></script>
    </body>
    </html> -->

    <!DOCTYPE html>
<html lang="en">
    <head><meta charset="utf-8"
        ><meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="/favicon.ico"><title>datalize-ui</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/app.css" rel="preload" as="style">
        <link href="css/chunk-vendors.css" rel="preload" as="style">
        <link href="js/app.js" rel="preload" as="script">
        <link href="js/chunk-vendors.js" rel="preload" as="script">
        <link href="css/chunk-vendors.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
    </head>
    <body id="app">
        <noscript><strong>We're sorry but datalize-ui doesn't work properly without JavaScript enabled. Please enable it to continue.

        </strong></noscript>
        <script src="js/chunk-vendors.js"></script>
        <script src="js/app.js"></script>
    </body>
    </html>